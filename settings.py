from openpyxl.utils import column_index_from_string
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
engine = create_engine('mysql+mysqldb://g39-vote:g39-vote@172.29.21.112/g39_wwwdbt?charset=utf8', pool_recycle=3600)

print(engine)

VOTE_PREFIX = 'VOTE_2017_2_'
DATE_START = '2017-07-01 00:00:00'
DATE_END = '2017-12-31 23:59:59'

START_ROW = 16
END_ROW = 47
MAX_COLUMN = column_index_from_string('CO')

QUESTS = {
    1: 'Организация транспортного обслуживания в муниципальном образовании',
    2: 'Качество автомобильных дорог в муниципальном образовании',
    3: 'Организация теплоснабжения (снабжение населения топливом)',
    4: 'Организация водоснабжения',
    5: 'Организация электроснабжения',
    6: 'Организация газоснабжения',
}
