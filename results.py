import sys
from sqlalchemy.orm import sessionmaker

from models.answer import Answer
from models.channel import *
from models.question import Question
from models.vote import Vote
from models.event import *
from parser import get_data
from settings import *

Session = sessionmaker(bind=engine)
session = Session()

votes = session.query(Channel).filter(Channel.name.like('VOTE_2016%'))
votes_id = set()
# for vote in votes:
#     votes_id.add(vote.id)


# print(votes_id)
# exit()
for channel in session.query(Channel).filter(Channel.name.like('%2016%')):
    for vote in session.query(Vote).filter(Vote.channel == channel):
        votes_id.add(vote.id)
# for question in session.query(Question).filter(Question.vote == vote):
#             for answer in session.query(Answer).filter(Answer.question == question):
#                 if

events_id = set()
for event in session.query(Event).filter(Event.vote_id.in_(votes_id)).filter(Event.valid == 'Y'):
    # print(event.date_vote)
    events_id.add(event.id)

event_questions_id = set()
for event_question in session.query(EventQuestion).filter(EventQuestion.event_id.in_(events_id)):
    event_questions_id.add(event_question.id)

event_answers = set()
# counter = 0


event_answer_ids = set()
summary_vote = {}
for event_answer in session.query(EventAnswer).filter(EventAnswer.event_question_id.in_(event_questions_id)):
    channel = event_answer.answer.question.vote.channel
    if not summary_vote.get(channel.title):
        summary_vote[channel.title] = {}
    vote = event_answer.answer.question.vote
    if not summary_vote[channel.title].get(vote.title):
        summary_vote[channel.title][vote.title] = {}
        summary_vote[channel.title][vote.title]['positive'] = 0
        summary_vote[channel.title][vote.title]['count'] = 0
        summary_vote[channel.title][vote.title]['description'] = vote.description

    question = event_answer.answer.question
    if not summary_vote[channel.title][vote.title].get(question.title):
        summary_vote[channel.title][vote.title][question.title] = []
    answer = event_answer.answer
    summary_vote[channel.title][vote.title][question.title].append(answer.title)
    if int(answer.title) >= 3:
        summary_vote[channel.title][vote.title]['positive'] += 1
    summary_vote[channel.title][vote.title]['count'] += 1

for channel_name, channel in summary_vote.items():
    for vote_name, vote in channel.items():
        # print(vote)
        print("%s\t%s\t%s\t%s" % (channel_name, vote_name, vote['count'], vote['positive']))

# print("%s %s %s %s" % (event_answer.answer.question.vote.channel.title, event_answer.answer.question.vote.title,
#                        event_answer.answer.question.title, event_answer.answer.title))
# counter += 1
