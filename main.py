import sys
from sqlalchemy.orm import sessionmaker

from models.answer import Answer
from models.channel import *
from models.question import Question
from models.vote import Vote
from parser import get_data
from settings import *


def create_votes(votes):
    """ This is main function, it parse and create votes in Bitrix DB"""
    votes_array = votes

    Session = sessionmaker(bind=engine)
    session = Session()

    for place, votes_row in votes_array.items():
        channel = Channel(name=place, sorting=100, active='Y', title=votes_row['ru'], vote_single='N', captcha='N',
                          hidden='N')
        session.add(channel)
        session.commit()
        print(channel.id)
        channel2site = {Channel2Site(channel_id=channel.id, site='s1'),
                        Channel2Site(channel_id=channel.id, site='s2'),
                        }

        channel2group = {
            Channel2Group(channel_id=channel.id, group_id=2, perm=2),
            Channel2Group(channel_id=channel.id, group_id=3, perm=2),
            Channel2Group(channel_id=channel.id, group_id=4, perm=2),
            Channel2Group(channel_id=channel.id, group_id=5, perm=2),
            Channel2Group(channel_id=channel.id, group_id=6, perm=2),
            Channel2Group(channel_id=channel.id, group_id=11, perm=2),
            Channel2Group(channel_id=channel.id, group_id=12, perm=2),
            Channel2Group(channel_id=channel.id, group_id=13, perm=2),
        }

        session.add_all(channel2site)
        session.add_all(channel2group)
        session.commit()
        for v in votes_row['votes']:
            des = ''
            if 'time' in v:
                # des = v['unit'] + ' (' + v['post'] + ', срок полномочий - ' + str(v['time']) + ')'
                des = v['unit'] + ' (' + v['post'] + ')'
            else:
                des = v['unit'] + '(' + str(v['post']) + ')'
            vote = Vote(channel_id=channel.id, title=v['name'], description=des)
            session.add(vote)
            session.commit()

            if 'quest' in v and v['quest']:
                for q in v['quest']:
                    quest = Question(vote_id=vote.id, title=QUESTS[q])
                    session.add(quest)
                    session.commit()
                    for i in range(1, 6):
                        sort = i * 100
                        answer = Answer(question_id=quest.id, sorting=sort, title=i)
                        session.add(answer)
                        session.commit()

        print('опрос %s добавлен' % place)


def init():
    votes = get_data()
    create_votes(votes)


if __name__ == "__main__":
    init()
