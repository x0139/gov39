import re

from openpyxl import load_workbook
from openpyxl.utils import column_index_from_string
from transliterate import translit

from settings import START_ROW, END_ROW, VOTE_PREFIX

number_re = re.compile('[^a-zA-Z0-9]+')


def clean_number(number):
    return number_re.sub('', number)


def questions(ws, row, col_start, col_end):
    questions = []
    range_start = column_index_from_string(col_start)
    # print(range_start)
    range_end = column_index_from_string(col_end)
    # print(range_end)
    for column in range(range_start, range_end):
        data = ws.cell(row=row, column=column).value
        # print("%s %s%s" % (data, row, column))
        if data:
            questions.append(column - (range_start - 1))
    if len(questions) > 0:
        return questions
    else:
        return False


def get_data():
    vote_file = 'vote.xlsx'
    wb = load_workbook(filename=vote_file, read_only=True)
    ws = wb.get_sheet_by_name('Лист1')
    votes = {}

    for row in range(START_ROW, END_ROW + 1):
        place = ''

        unit_1, unit_2, unit_3, unit_4, unit_5, unit_6, unit_7, unit_8, unit_9, unit_10 = {}, {}, {}, {}, {}, {}, {}, {}, {}, {}
        votes_row = []

        place_raw = ws.cell(row=row, column=column_index_from_string('B')).value

        if place_raw:
            place = VOTE_PREFIX + clean_number(translit(place_raw, 'ru', reversed=True))
        # todo make it recursive
        else:
            place_raw = ws.cell(row=row - 1, column=column_index_from_string('B')).value
            place = VOTE_PREFIX + clean_number(translit(place_raw, 'ru', reversed=True))
            votes_row = votes.get(place)['votes']
            # print(votes_row)

        unit_1 = {
            'unit': ws.cell(row=row, column=column_index_from_string('C')).value,
            'post': ws.cell(row=row, column=column_index_from_string('D')).value,
            'name': ws.cell(row=row, column=column_index_from_string('E')).value,
            'time': ws.cell(row=row, column=column_index_from_string('F')).value,
            'quest': questions(ws=ws, row=row, col_start='G', col_end='M'),

        }

        print(unit_1['quest'])

        if unit_1.get('unit'):
            votes_row.append(unit_1)

        unit_2 = {
            'unit': ws.cell(row=row, column=column_index_from_string('M')).value,
            'post': ws.cell(row=row, column=column_index_from_string('N')).value,
            'name': ws.cell(row=row, column=column_index_from_string('O')).value,
            'time': ws.cell(row=row, column=column_index_from_string('P')).value,
            'quest': questions(ws=ws, row=row, col_start='Q', col_end='W'),
        }
        if unit_2.get('unit'):
            votes_row.append(unit_2)

        unit_3 = {
            'unit': ws.cell(row=row, column=column_index_from_string('W')).value,
            'post': ws.cell(row=row, column=column_index_from_string('X')).value,
            'name': ws.cell(row=row, column=column_index_from_string('Y')).value,
            'time': ws.cell(row=row, column=column_index_from_string('Z')).value,
            'quest': questions(ws=ws, row=row, col_start='AA', col_end='AE'),
        }
        if unit_3.get('unit'):
            votes_row.append(unit_3)

        unit_4 = {
            'unit': ws.cell(row=row, column=column_index_from_string('AF')).value,
            'post': ws.cell(row=row, column=column_index_from_string('AG')).value,
            'name': ws.cell(row=row, column=column_index_from_string('AH')).value,
            'time': ws.cell(row=row, column=column_index_from_string('AI')).value,
            'quest': questions(ws=ws, row=row, col_start='AJ', col_end='AO'),
        }
        if unit_4.get('unit'):
            votes_row.append(unit_4)

        unit_5 = {
            'unit': ws.cell(row=row, column=column_index_from_string('AO')).value,
            'post': ws.cell(row=row, column=column_index_from_string('AP')).value,
            'name': ws.cell(row=row, column=column_index_from_string('AQ')).value,
            'time': ws.cell(row=row, column=column_index_from_string('AR')).value,
            'quest': questions(ws=ws, row=row, col_start='AS', col_end='AX'),
        }
        if unit_5.get('unit'):
            votes_row.append(unit_5)

        unit_6 = {
            'unit': ws.cell(row=row, column=column_index_from_string('AX')).value,
            'post': ws.cell(row=row, column=column_index_from_string('AY')).value,
            'name': ws.cell(row=row, column=column_index_from_string('AZ')).value,
            'time': ws.cell(row=row, column=column_index_from_string('BA')).value,
            'quest': questions(ws=ws, row=row, col_start='BB', col_end='BG'),
        }
        if unit_6.get('unit'):
            votes_row.append(unit_6)

        unit_7 = {
            'unit': ws.cell(row=row, column=column_index_from_string('BG')).value,
            'post': ws.cell(row=row, column=column_index_from_string('BH')).value,
            'name': ws.cell(row=row, column=column_index_from_string('BI')).value,
            'time': ws.cell(row=row, column=column_index_from_string('BJ')).value,
            'quest': questions(ws=ws, row=row, col_start='BK', col_end='BP'),
        }
        if unit_7.get('unit'):
            votes_row.append(unit_7)

        unit_8 = {
            'unit': ws.cell(row=row, column=column_index_from_string('BP')).value,
            'post': ws.cell(row=row, column=column_index_from_string('BQ')).value,
            'name': ws.cell(row=row, column=column_index_from_string('BR')).value,
            'time': ws.cell(row=row, column=column_index_from_string('BS')).value,
            'quest': questions(ws=ws, row=row, col_start='BT', col_end='BY'),
        }
        if unit_8.get('unit'):
            votes_row.append(unit_8)

        unit_9 = {
            'unit': ws.cell(row=row, column=column_index_from_string('BY')).value,
            'post': ws.cell(row=row, column=column_index_from_string('BZ')).value,
            'name': ws.cell(row=row, column=column_index_from_string('CA')).value,
            'time': ws.cell(row=row, column=column_index_from_string('CB')).value,
            'quest': questions(ws=ws, row=row, col_start='CC', col_end='CH'),
        }
        if unit_9.get('unit'):
            votes_row.append(unit_9)

        unit_10 = {
            'unit': ws.cell(row=row, column=column_index_from_string('CH')).value,
            'post': ws.cell(row=row, column=column_index_from_string('CI')).value,
            'name': ws.cell(row=row, column=column_index_from_string('CJ')).value,
            'quest': questions(ws=ws, row=row, col_start='CK', col_end='CP'),
        }
        if unit_10.get('unit'):
            votes_row.append(unit_10)

        votes.update({place: {'ru': place_raw, 'votes': votes_row}})

    print(votes)
    return votes
