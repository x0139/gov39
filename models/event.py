from sqlalchemy import func
from sqlalchemy import Column, Integer, String, ForeignKey, CHAR, DATETIME, VARCHAR
from sqlalchemy.orm import relationship

from settings import Base


class Event(Base):
    __tablename__ = 'b_vote_event'

    id = Column('id', Integer, primary_key=True)
    vote_id = Column('vote_id', Integer, ForeignKey('b_vote.id'))
    vote = relationship('Vote')
    vote_user_id = Column('vote_user_id', Integer)
    date_vote = Column('date_vote', DATETIME)
    # stat_session_id =
    # ip =
    valid = Column('valid', CHAR(1))


class EventQuestion(Base):
    __tablename__ = 'b_vote_event_question'

    id = Column('id', Integer, primary_key=True)
    event_id = Column('event_id', Integer, ForeignKey('b_vote_event.id'))
    event = relationship('Event')
    question_id = Column('question_id', Integer, ForeignKey('b_vote_question.id'))
    question = relationship('Question')


class EventAnswer(Base):
    __tablename__ = 'b_vote_event_answer'

    id = Column('id', Integer, primary_key=True)
    event_question_id = Column('event_question_id', Integer, ForeignKey('b_vote_event_question.id'))
    event_question = relationship('EventQuestion')
    answer_id = Column('answer_id', Integer, ForeignKey('b_vote_answer.id'))
    answer = relationship('Answer')
    # message =
