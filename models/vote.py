from sqlalchemy import func
from sqlalchemy import Column, Integer, String, ForeignKey, CHAR, DATETIME, VARCHAR
from sqlalchemy.orm import relationship

from settings import Base, DATE_START, DATE_END


class Vote(Base):
    __tablename__ = 'b_vote'

    id = Column('id', Integer, primary_key=True)
    channel_id = Column('channel_id', Integer, ForeignKey('b_vote_channel.id'))
    channel = relationship('Channel')
    sorting = Column('c_sort', Integer, default=100)
    active = Column('active', CHAR(1), default='Y')
    timestamp = Column('timestamp_x', DATETIME, default=func.now())
    date_start = Column('date_start', DATETIME, default=DATE_START)
    date_end = Column('date_end', DATETIME, default=DATE_END)
    counter = Column('counter', Integer, default=0)
    title = Column('title', VARCHAR(255))
    description = Column('description', String)
    description_type = Column('description_type', VARCHAR(4), default='html')
    image = Column('image_id', Integer)
    event1 = Column('event1', VARCHAR(255))
    event2 = Column('event2', VARCHAR(255))
    event3 = Column('event3', VARCHAR(255))
    unique_type = Column('unique_type', Integer, default=7)
    keep_ip_sec = Column('keep_ip_sec', Integer, default=86400)
    # delay = Column('delay', Integer, default=1)
    # delay_type = Column('delay_type', CHAR(1), default='D')
    template = Column('template', VARCHAR(255))
    result_template = Column('result_template', VARCHAR(255))
    notify = Column('notify', CHAR(1), default='N')
    author_id = Column('author_id', Integer, default=1)
    url = Column('url', VARCHAR(255))
