from sqlalchemy import func
from sqlalchemy import Column, Integer, String, ForeignKey, CHAR, DATETIME, VARCHAR
from sqlalchemy.orm import relationship

from settings import Base


class Channel(Base):
    __tablename__ = 'b_vote_channel'

    id = Column('id', Integer, primary_key=True)
    name = Column('symbolic_name', String)
    sorting = Column('c_sort', Integer)
    first_site = Column('first_site_id', CHAR(length=2))
    active = Column('active', CHAR(length=1))
    timestamp = Column('timestamp_x', DATETIME, default=func.now())
    title = Column('title', String)
    vote_single = Column('vote_single', CHAR(length=1))
    captcha = Column('use_captcha', CHAR(length=1))
    hidden = Column('hidden', CHAR(length=1))


class Channel2Site(Base):
    __tablename__ = 'b_vote_channel_2_site'

    channel_id = Column('channel_id', Integer, ForeignKey('b_vote_channel.id'), primary_key=True, autoincrement=False)
    channel = relationship("Channel")
    site = Column('site_Id', CHAR(length=2), primary_key=True)


class Channel2Group(Base):
    __tablename__ = 'b_vote_channel_2_group'

    id = Column('id', Integer, primary_key=True)
    channel_id = Column('channel_id', Integer, ForeignKey('b_vote_channel.id'), primary_key=True)
    channel = relationship("Channel")
    group_id = Column('group_id', Integer)
    perm = Column('permission', Integer)
