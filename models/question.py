from sqlalchemy import func
from sqlalchemy import Column, Integer, String, ForeignKey, CHAR, DATETIME, VARCHAR
from sqlalchemy.orm import relationship

from settings import Base


class Question(Base):
    __tablename__ = 'b_vote_question'

    id = Column('id', Integer, primary_key=True)
    active = Column('active', CHAR(1), default='Y')
    timestamp = Column('timestamp_x', DATETIME, default=func.now())
    vote_id = Column('vote_id', Integer, ForeignKey('b_vote.id'))
    vote = relationship("Vote")
    sorting = Column('c_sort', Integer, default=100)
    counter = Column('counter', Integer, default=0)
    title = Column('question', String)
    type = Column('question_type', VARCHAR(4), default='html')
    image = Column('image_id', Integer)
    diagram = Column('diagram', CHAR(1), default='Y')
    required = Column('required', CHAR(1), default='Y')
    diagram_type = Column('diagram_type', VARCHAR(10), default='Y')
    template = Column('template', VARCHAR(255))
    template_new = Column('template_new', VARCHAR(255))
