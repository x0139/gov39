from sqlalchemy import func
from sqlalchemy import Column, Integer, String, ForeignKey, CHAR, DATETIME, VARCHAR
from sqlalchemy.orm import relationship

from settings import Base

class Answer(Base):
    __tablename__ = 'b_vote_answer'

    id = Column('id', Integer, primary_key=True)
    active = Column('active',CHAR(1), default='Y')
    timestamp = Column('timestamp_x', DATETIME, default=func.now())
    question_id = Column('question_id', Integer, ForeignKey('b_vote_question.id'))
    question = relationship('Question')
    sorting = Column('c_sort', Integer)
    title = Column('message', String)
    counter = Column('counter', Integer, default=0)
    field_type = Column('field_type', Integer, default=0)
    field_width = Column('field_width', Integer, default=0)
    field_height = Column('field_height', Integer, default=0)
    field_param = Column('field_param', VARCHAR(255))
    color = Column('color', VARCHAR(7))
    message_type = Column('message_type', VARCHAR(4), default='html')